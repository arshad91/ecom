package com.ars.ecom.model;

public class Item {
    private String productId;
    private int quantity;
    private double unitCost;

    public Item(){}

    public Item(String productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
//        this.unitCost = unitCost;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }
}
