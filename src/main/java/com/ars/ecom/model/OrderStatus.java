package com.ars.ecom.model;

public enum OrderStatus{
    INPROGRESS,
    SUCCESS,
    FAILURE
}