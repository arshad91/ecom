package com.ars.ecom.model;

public class Product {
    private String productId;
    private String productName;
    private double unitCost;
    private String productDescription;

    public Product(){}

    public Product(String productId, String productName, double unitCost, String productDescription) {
        this.productId = productId;
        this.productName = productName;
        this.unitCost = unitCost;
        this.productDescription = productDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }


}
