package com.ars.ecom.model;

import java.util.List;

public class ShoppingCart {

    private String cartId;
    private List<Item> items;

    public ShoppingCart(){}

    public ShoppingCart(String cartId, List<Item> items) {
        this.cartId = cartId;
        this.items = items;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
