package com.ars.ecom.service.businesspolicy.order;

import com.ars.ecom.service.businesspolicy.IBusinessPolicy;
import org.springframework.stereotype.Service;

@Service

public class OrderBusinessPolicy implements IBusinessPolicy{

    @Override
    public boolean validate(String id) {
        //business specific policy
        return true;
    }
}
