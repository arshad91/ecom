package com.ars.ecom.service.businesspolicy;

public interface IBusinessPolicy{
    boolean validate(String id);
}
