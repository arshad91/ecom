package com.ars.ecom.service.businesspolicy.product;

import com.ars.ecom.service.businesspolicy.IBusinessPolicy;
import org.springframework.stereotype.Service;

@Service
public class ProductBusinessPolicy implements IBusinessPolicy {
    @Override
    public boolean validate(String id) {
        //business specific policy
        return true;
    }
}
