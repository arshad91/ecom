package com.ars.ecom.service.cart;

import com.ars.ecom.model.*;
import com.ars.ecom.repository.CartRepository;
import com.ars.ecom.repository.CustomerRepository;
import com.ars.ecom.repository.OrderRepository;
import com.ars.ecom.repository.ProductRepository;
import com.ars.ecom.service.businesspolicy.cart.CartBusinessPolicy;
import com.ars.ecom.service.payment.PaymentService;
import com.ars.ecom.sessionmanager.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CartService {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    CartBusinessPolicy businessPolicy;

    @Autowired
    Session session;

    @Autowired
    PaymentService paymentService;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CustomerRepository customerRepository;

    public List<Item> viewCart() {
        if(!businessPolicy.validate(session.getCartId())){
            return new ArrayList<>();
        }
        return cartRepository.viewCart(session.getCartId());
    }

    public Item addItemInCart(Item item) {
        if(!businessPolicy.validate(item.getProductId())){
            return null;
        }
        return cartRepository.addItemToCart(session.getCartId(), item);
    }

    public String deleteItem(String productId) {
        if(!businessPolicy.validate(productId)){
            return null;
        }
        String product = cartRepository.deleteItem(session.getCartId(), productId);
        if(product == null)
            return "Product "+ productId + "does not exist";
        return product;
    }

    public Order checkout(String cartId){
        //check product availability
        List<Item> items = cartRepository.viewCart(cartId);
        if(items == null || items.size() == 0){
            return orderFailed();
        }
        for(Item item : cartRepository.viewCart(cartId)){
            int quantity = item.getQuantity();
            if(productRepository.getProductCountFromInventory(item.getProductId())<quantity){
                return orderFailed();
            }
        }

        double totalAmount = calculateTotalPrice(cartId);
        boolean paymentStatus = paymentService.processPayment(totalAmount);
        if(paymentStatus){
            return updateData(cartId);
        }else {
            return orderFailed();
        }
    }

    private Order orderFailed(){
        Order order = new Order();
        order.setOrderStatus(OrderStatus.FAILURE);
        return order;
    }
    private Order updateData(String cartId) {
        List<Item> items = cartRepository.viewCart(cartId);
        for(Item item : items){
            int quantity = item.getQuantity();
            if(productRepository.getProductCountFromInventory(item.getProductId())<quantity){
                return orderFailed();
            }else {
                Product product = productRepository.deleteProduct(item.getProductId());
                if(product == null){
                    return orderFailed();
                }
            }
        }

        //update order repo
        String userId =  session.getUserId();
        Customer customer = customerRepository.getCustomer(userId);
        double totalAmount = calculateTotalPrice(cartId);

        Order order = new Order(null, items, customer.getName(), customer.getAddress(), new Date(), totalAmount);
        order.setOrderStatus(OrderStatus.SUCCESS);
        cartRepository.removeAllItemsFromCart(cartId);
        return orderRepository.addOrder(order);
    }


    public double calculateTotalPrice(String cartId){
       List<Item> list = cartRepository.viewCart(cartId);
       double totalPrice = 0;
       for(Item item:list){
           totalPrice += item.getQuantity() * item.getUnitCost();
       }
       return totalPrice;
    }
}
