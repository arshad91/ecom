package com.ars.ecom.service.product;

import com.ars.ecom.model.Product;
import com.ars.ecom.repository.ProductRepository;
import com.ars.ecom.service.businesspolicy.product.ProductBusinessPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductBusinessPolicy businessPolicy;


    public List<Product> getAllProducts() {
       return productRepository.allProducts();
    }

    public Product getProduct(String productId) {
        if(!businessPolicy.validate(productId)){
            return null;
        }
        Product product = productRepository.deleteProduct(productId);
        if(product == null)
            return new Product();

        return product;
    }

    public Product addProduct(Product product) {
        if(!businessPolicy.validate(product.getProductId())){
            return new Product();
        }
        return productRepository.addProduct(product);
    }

    public String deleteProduct(String productId) {
        if(!businessPolicy.validate(productId)){
            return "Business policy violated";
        }
        Product product = productRepository.deleteProduct(productId);
        if(product == null)
            return "Product "+ productId+ " does not exist";

        return product.getProductId();
    }
}
