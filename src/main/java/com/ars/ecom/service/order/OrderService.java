package com.ars.ecom.service.order;

import com.ars.ecom.model.Order;
import com.ars.ecom.repository.ProductRepository;
import com.ars.ecom.service.businesspolicy.order.OrderBusinessPolicy;
import com.ars.ecom.service.cart.CartService;
import com.ars.ecom.sessionmanager.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    CartService cartService;

    @Autowired
    OrderBusinessPolicy orderBusinessPolicy;

    @Autowired
    Session session;

    public Order placeOrder() {
        String cartId = session.getCartId();
        if(!orderBusinessPolicy.validate(cartId)){
            return null;
        }

        Order order =  cartService.checkout(cartId);
        return order;
    }
}
