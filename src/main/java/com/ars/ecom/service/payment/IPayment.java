package com.ars.ecom.service.payment;

public interface IPayment {

    public boolean processPayment(double amount);
}
