package com.ars.ecom.sessionmanager;

import org.springframework.stereotype.Service;

@Service
public class Session {

    //Session manager will fetch actual session related info
    public static String getUserId(){
        //mock implementation
        return "user1";
    }

    public static String getCartId(){
        //mock implementation
        return "cart1";
    }
}
