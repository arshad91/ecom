package com.ars.ecom.controller;

import com.ars.ecom.model.Item;
import com.ars.ecom.model.Order;
import com.ars.ecom.model.Product;
import com.ars.ecom.service.order.OrderService;
import com.ars.ecom.service.product.ProductService;
import com.ars.ecom.service.cart.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EcomControlller {

    @Autowired
    ProductService productService;

    @Autowired
    CartService cartService;

    @Autowired
    OrderService orderService;


    @RequestMapping(value = "/product", method = RequestMethod.GET)
    private List<Product> getAllProducts(){
        return productService.getAllProducts();
    }


    @RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
    private Product getProduct(@PathVariable("productId") String productId){
        return productService.getProduct(productId);
    }


    @RequestMapping(value = "/product", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Product addProduct(@RequestBody Product product){
        return productService.addProduct(product);
    }


    @RequestMapping(value = "/product/{productId}", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
    private String deleteProduct(@PathVariable("productId") String productId){
        return productService.deleteProduct(productId);
    }


    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    private List<Item> viewCart(){
        return cartService.viewCart();
    }


    @RequestMapping(value = "/cart", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Item addItem(@RequestBody Item item){
        return cartService.addItemInCart(item);
    }


    @RequestMapping(value = "/cart/{productId}", method = RequestMethod.DELETE)
    private String addProduct(@PathVariable("productId") String productId){
        return cartService.deleteItem(productId);
    }


    @RequestMapping(value = "/placeorder", method = RequestMethod.POST)
    private Order placeOrder(){
        return orderService.placeOrder();
    }

}
