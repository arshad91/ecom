package com.ars.ecom.repository;

import com.ars.ecom.model.Order;
import com.ars.ecom.model.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderRepository {
    private static Integer id = 1;
    private static Map<String, Order> orderMap = new HashMap<>();

   public Order addOrder(Order order){
       String orderId = "OD00"+id++;
       order.setOrderId(orderId);
       orderMap.put(orderId, order);
       return order;
   }
}
