package com.ars.ecom.repository;

import com.ars.ecom.model.Address;
import com.ars.ecom.model.Customer;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CustomerRepository {
    private static Map<String, Customer> userMap = new HashMap<>();

    static {
        Customer customer = new Customer();
        customer.setName("ars");
        customer.setEmail("user1@gmai.com");
        customer.setPhone("9123456780");
        customer.setAddress(new Address("101, line1", "mumbai", "mumbai", "400001", "Maharashtra", "India"));
        userMap.put("user1", customer);
    }

    public Customer getCustomer(String userId){
        return userMap.get(userId);
    }

}
