package com.ars.ecom.repository;

import com.ars.ecom.model.Item;
import com.ars.ecom.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CartRepository {
    private static Map<String, List<Item>> cartItemsMap = new HashMap<>();

    @Autowired
    ProductRepository productRepository;

    public Item addItemToCart(String cartId, Item item){
        Product product = productRepository.getProduct(item.getProductId());
        if(product == null){
            return new Item();
        }

        List<Item> items = cartItemsMap.get(cartId);
        if(items == null)
            items = new ArrayList<>();

        item.setUnitCost(productRepository.getProduct(item.getProductId()).getUnitCost());
        items.add(item);
        cartItemsMap.put(cartId, items);
        return item;
    }

    public List<Item> viewCart(String cartId){
        List<Item> items = cartItemsMap.get(cartId);
        if(items == null || items.size() == 0)
            return new ArrayList<>();
        return items;
    }

    public String deleteItem(String cartId, String productId) {
        List<Item> items = cartItemsMap.get(cartId);
        List<Item> list = items.stream().filter(x->!(x.getProductId().equals(productId))).collect(Collectors.toList());

        if(list == null || list.size() == 0 ){
            cartItemsMap.remove(cartId);
        }
        else{
            cartItemsMap.put(cartId,list);
        }
        return productId;
    }

    public List<Item> removeAllItemsFromCart(String cartId) {
        return cartItemsMap.remove(cartId);
    }
}
