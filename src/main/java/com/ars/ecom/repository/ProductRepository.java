package com.ars.ecom.repository;

import com.ars.ecom.model.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ProductRepository {
    private static Map<String, Product> productMap = new ConcurrentHashMap<>();
    private static Map<String, Integer> inventoryMap = new ConcurrentHashMap<>();

    public Product addProduct(Product product){
        String productId = product.getProductId();
        inventoryMap.put(productId, inventoryMap.getOrDefault(productId,0)+1);
        productMap.put(productId, product);
        return product;
    }

    public List<Product> allProducts(){
        return new ArrayList(productMap.values());
    }

    public Product getProduct(String productId){
        return productMap.get(productId);
    }

    public Product deleteProduct(String productId) {
        Product product = productMap.get(productId);
        if(product == null)
            return null;

        if(inventoryMap.get(productId) <= 1){
            inventoryMap.remove(productId);
            productMap.remove(productId);
        }
        else
            inventoryMap.put(productId, inventoryMap.get(productId)-1);
        return product;
    }

    public int getProductCountFromInventory(String productId){
        return inventoryMap.getOrDefault(productId,0);
    }

}
